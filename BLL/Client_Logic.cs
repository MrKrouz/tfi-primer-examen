﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;

namespace BLL
{
    public class Client_Logic
    {
        DAL.Client_Mapper mapper = new DAL.Client_Mapper();
        Company_Logic companyBLL = new Company_Logic();
        public int Add(BE.Client client)
        {
            if (string.IsNullOrEmpty(client.Name))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(client.LastName))
            {
                return 0;
            }
            else if (client.Company is null)
            {
                return 0;
            }
            else
            {
                return mapper.Create(client);
            }
        }

        public List<BE.Client> GetAllClients()
        {
            List<BE.Client> clients = mapper.Read();
            foreach (BE.Client client in clients)
            {
                client.Company = companyBLL.GetCompanyByID(client.Company.ID);
            }

            return clients;
        }

        public List<BE.Client> GetActiveClients()
        {
            List<BE.Client> clients = mapper.Read().Where(c => c.Active == true).ToList();
            foreach (BE.Client client in clients)
            {
                client.Company = companyBLL.GetCompanyByID(client.Company.ID);
            }

            return clients;
        }

        public BE.Client GetClientByID(int clientID)
        {
            return GetAllClients().Where(c => c.ID == clientID).FirstOrDefault();
        }
        public int Delete(BE.Client client)
        {
            return mapper.Delete(client);
        }
        public int Update(BE.Client client)
        {
            if (string.IsNullOrEmpty(client.Name))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(client.LastName))
            {
                return 0;
            }
            else if (client.Company is null)
            {
                return 0;
            }
            else
            {
                return mapper.Update(client);
            }
        }

    }
}
