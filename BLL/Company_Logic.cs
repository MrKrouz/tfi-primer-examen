﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data.SqlClient;
using BE;

namespace BLL
{
    public class Company_Logic
    {
        DAL.Company_Mapper mapper = new Company_Mapper();
        public int Add(BE.Company company)
        {
            if (string.IsNullOrEmpty(company.Name))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(company.Country))
            {
                return 0;
            }
            return mapper.Create(company);
        }
        public List<BE.Company> GetCompanies()
        {
            return mapper.Read();
        }

        public BE.Company GetCompanyByID(int companyID)
        {
            return GetCompanies().Where(c => c.ID == companyID).FirstOrDefault();
        }
        public int Update(BE.Company company)
        {
            if (string.IsNullOrEmpty(company.Name))
            {
                return 0;
            }
            else if (string.IsNullOrEmpty(company.Country))
            {
                return 0;
            }
            return mapper.Update(company);
        }
        public int Delete (BE.Company company)
        {
            return mapper.Delete(company);
        }
    }
}
