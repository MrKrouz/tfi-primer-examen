﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;


namespace BLL
{
    public class Registry_Logic
    {
        DAL.Registry_Mapper mapper = new Registry_Mapper();
        Client_Logic clientBLL = new Client_Logic();
        public int Add(BE.CallRegistry callRegistry)
        {
            return mapper.Create(callRegistry);
        }

        public List<BE.CallRegistry> GetRegistries()
        {
            List<BE.CallRegistry> callRegistries = mapper.Read();
            foreach(BE.CallRegistry callRegistry in callRegistries)
            {
                callRegistry.Origin = clientBLL.GetClientByID(callRegistry.Origin.ID);
                callRegistry.Destination = clientBLL.GetClientByID(callRegistry.Destination.ID);
            }
            return callRegistries;
        }


        //Punto 4
        public double GetMontlyDebtByClient(int clientID)
        {
            double total = 1000;
            // Takes only this month registry.
            foreach (BE.CallRegistry registry in GetRegistries().Where(m => m.Date.Month == DateTime.Now.Month).Where(c=> c.Origin.ID == clientID))
            {
                // If companies are different.
                if (registry.Origin.Company.ID != registry.Destination.Company.ID)
                {
                    //  If countries are different.
                    if(registry.Origin.Company.Country != registry.Destination.Company.Country)
                    {
                        total = total + (registry.Duration * 0.95) + 3;
                    }
                    // If within the same country but different type. Type being phone or cellphone.
                    else if ((int)registry.Origin.ClientType != (int)registry.Destination.ClientType)
                    {
                        total = total + (registry.Duration * 0.95);
                    }
                    // If within the same country, and the same type, but different company.
                    else
                    {
                        total = total + (registry.Duration * 0.80);
                    }
                }
            }

            return total;
        }


        //Punto 5 y 6
        public Dictionary<string, double> GetTotalByMonth(int month)
        {
            var registries = GetRegistries().Where(m => m.Date.Month == month);
            double total = 1000 * registries.Count();
            double totalInternacional = 0;
            double totalTipos = 0;
            double totalCompanias = 0;
            double totalMismaCompania = 0;

            Dictionary<string, double> info = new Dictionary<string, double>();

            // Takes only the data regarding the month required.
            foreach(BE.CallRegistry registry in registries)
            {
                // If companies are different
                if (registry.Origin.Company.ID != registry.Destination.Company.ID)
                {
                    //  If countries are different.
                    if (registry.Origin.Company.Country != registry.Destination.Company.Country)
                    {
                        totalInternacional = totalInternacional + 1000 + (registry.Duration * 0.95) + 3;
                        total = total + (registry.Duration * 0.95) + 3;
                    }
                    // If within the same country but different type. Type being phone or cellphone.
                    else if ((int)registry.Origin.ClientType != (int)registry.Destination.ClientType)
                    {
                        totalTipos = totalTipos + 1000 + (registry.Duration * 0.95);
                        total = total + (registry.Duration * 0.95) + 3;

                    }
                    // If within the same country, and the same type, but different company.
                    else
                    {
                        totalCompanias = totalCompanias + 1000 + (registry.Duration * 0.80);
                        total = total + (registry.Duration * 0.95) + 3;

                    }
                }
                else
                {
                    totalMismaCompania = totalMismaCompania + 1000;
                }
            }
            info.Add("sameCompany", totalMismaCompania);
            info.Add("differentCompany", totalCompanias);
            info.Add("international", totalInternacional);
            info.Add("differentType", totalTipos);
            info.Add("total", total);

            return info;
        }

        // Punto 7 general
        public Dictionary<string, int> GetAllCallsByType()
        {
            Dictionary<string, int> callsByType = new Dictionary<string, int>();
            int sameCompany = 0;
            int differentCompany = 0;
            int differentType = 0;
            int international = 0;
            foreach (BE.CallRegistry registry in GetRegistries())
            {
                // If companies are different.
                if (registry.Origin.Company.ID != registry.Destination.Company.ID)
                {
                    differentCompany++;
                    //  If countries are different.
                    if (registry.Origin.Company.Country != registry.Destination.Company.Country)
                    {
                        international++;
                    }
                    // If within the same country but different type. Type being phone or cellphone.
                    else if ((int)registry.Origin.ClientType != (int)registry.Destination.ClientType)
                    {
                        differentType++;
                    }
                }
                // If companies are the same.
                else
                {
                    sameCompany++;
                }
            }

            callsByType.Add("sameCompany", sameCompany);
            callsByType.Add("differentCompany", differentCompany);
            callsByType.Add("differentType", differentType);
            callsByType.Add("international", international);

            return callsByType;
        }


        //Punto 7 Por Cliente
        public Dictionary<string, int> GetAllCallsByClientID(int clientID)
        {
            Dictionary<string, int> callsByClient = new Dictionary<string, int>();
            int sameCompany = 0;
            int differentCompany = 0;
            int differentType = 0;
            int international = 0;
            foreach(BE.CallRegistry registry in GetRegistries().Where(c=> c.Origin.ID == clientID))
            {
                // If companies are different.
                if (registry.Origin.Company.ID != registry.Destination.Company.ID)
                {
                    differentCompany++;
                    //  If countries are different.
                    if (registry.Origin.Company.Country != registry.Destination.Company.Country)
                    {
                        international++;
                    }
                    // If within the same country but different type. Type being phone or cellphone.
                    else if ((int)registry.Origin.ClientType != (int)registry.Destination.ClientType)
                    {
                        differentType++;
                    }
                }
                // If companies are the same.
                else
                {
                    sameCompany++;
                }
            }

            callsByClient.Add("sameCompany", sameCompany);
            callsByClient.Add("differentCompany", differentCompany);
            callsByClient.Add("differentType", differentType);
            callsByClient.Add("international", international);

            return callsByClient;
        }


    }
}
