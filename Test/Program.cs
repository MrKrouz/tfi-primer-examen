﻿using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            BLL.Registry_Logic registry_Logic = new BLL.Registry_Logic();

            foreach (var item in registry_Logic.GetAllCallsByType())
            {
                Console.WriteLine("Company " + item.Key + " " + item.Value);
            }

            foreach (var item in registry_Logic.GetCallsByClientID(7))
            {
                Console.WriteLine("Client " + item.Key + " " + item.Value);
            }

            Console.WriteLine(registry_Logic.GetMontlyTotalByPromotion(0, 9));


        }
    }
}
