﻿<%@ Page Title="Gestión de cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Client.aspx.cs" Inherits="WebUI.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Gestión de cliente</h2>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="row">
        <div class="col-md-12">
            <p>
                <center>
                    A continuacion podra realizar la gestion de los clientes en sistema.
                </center>
            </p>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="lblUsername" runat="server" Text="Nombre"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label1" runat="server" Text="Apellido"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label2" runat="server" Text="Compañia"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label3" runat="server" Text="Tipo de servicio"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label4" runat="server" Text="Activo"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:CheckBox ID="CheckBox1" runat="server"></asp:CheckBox>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 3em">
                <div class="col-xs-3">
                </div>
                <div class="col-xs-3">
                    <asp:Button ID="Button1" class="btn btn-primary" runat="server" Text="Agregar" OnClick="Button1_Click" />
                </div>
                <div class="col-xs-3">
                    <asp:Button ID="Button2" class="btn btn-secondary" runat="server" Text="Modificar" OnClick="Button2_Click" />
                </div>
                <div class="col-xs-3">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 3em">
        <div class="col-md-12">
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-6">
                    <asp:GridView ID="GridView1" runat="server" class="table table-bordered table-condensed table-responsive table-hover" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" Width="100%">
                        <Columns>
                            <asp:BoundField HeaderText="ID" DataField="ID" ItemStyle-Width="2em" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="2em"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Nombre" DataField="Name" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Apellido" DataField="LastName" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Tipo de cliente" DataField="ClientType" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Compañia" DataField="Company.Name" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:CheckBoxField HeaderText="Activo" DataField="Active" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:CheckBoxField>
                            <asp:ButtonField CommandName="Seleccionar" Text="Seleccionar" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:ButtonField>
                            <asp:ButtonField CommandName="Eliminar" Text="Eliminar" />
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="col-xs-3">
                </div>
            </div>
        </div>
    </div>



    <div id="popup" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button4" class="btn btn-success" runat="server" Text="Button" OnClick="ButtonModal_Click" />
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function ShowPopup(title, body) {
            $("#popup .modal-title").html(title);
            $("#popup .modal-body").html(body);
            $("#popup").modal("show");
        }
    </script>
</asp:Content>
