﻿<%@ Page Title="Informacion de cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CompanyInfo.aspx.cs" Inherits="WebUI.CompanyInfo" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Informacion de compañia</h2>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="row">
        <div class="col-md-12">
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label12" runat="server" Text="Seleccione un mes"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label1" runat="server" Text="Recaudacion total"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="totalProfit" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label2" runat="server" Text="Recaudacion entre clientes de misma empresa"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="sameCompany" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label3" runat="server" Text="Recaudacion entre clientes de mismo tipo de telefono"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="samePhone" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>

            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label5" runat="server" Text="Recaudacion entre clientes de diferente pais"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="international" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>

            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label7" runat="server" Text="Recaudacion entre clientes de diferente tipo de telefono"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="otherPhone" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>

        </div>
    </div>
    <div class="row" style="padding-top: 2em">
        <p>
            <center>
                La informacion mostrada a continuacion esta tomada de forma anual.
            </center>
        </p>
        <div class="col-md-12">
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label16" runat="server" Text="Porcentaje de llamadas entre clientes de misma empresa"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="sameCompanyPercentage" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label20" runat="server" Text="Porcentaje de llamadas entre clientes de mismo tipo de telefono"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="samePhonePercentage" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label24" runat="server" Text="Porcentaje de llamadas entre clientes de diferente pais"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="internationalPercentage" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label28" runat="server" Text="Porcentaje de llamadas entre clientes de diferente tipo de telefono"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="otherPhonePercentage" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Button ID="Button2" class="btn btn-info" runat="server" Text="Generar Informacion" OnClick="Button1_Click" />
                </div>
                <div class="col-xs-3">
                </div>
                <div class="col-xs-3"></div>
            </div>
        </div>
    </div>

    <div id="popup" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button4" class="btn btn-success" runat="server" Text="Button" />
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function ShowPopup(title, body) {
            $("#popup .modal-title").html(title);
            $("#popup .modal-body").html(body);
            $("#popup").modal("show");
        }
    </script>
</asp:Content>
