﻿<%@ Page Title="Informacion de cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ClientInfo.aspx.cs" Inherits="WebUI.ClientInfo" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Informacion de cliente</h2>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="row">
        <div class="col-md-12">
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label12" runat="server" Text="Seleccione un cliente"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:DropDownList ID="DropDownList1" runat="server" ></asp:DropDownList>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="clientName" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label2" runat="server" Text="Apellido"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="clientLastName" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label3" runat="server" Text="Cargos del mes actual"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="clientMonthCharge" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label5" runat="server" Text="Cantidad de llamadas a misma compañia"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="sameCompany" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label7" runat="server" Text="Cantidad de llamadas internacionales"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="international" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label8" runat="server" Text="Cantidad de llamadas a otro tipo de telefono"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="otherType" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label10" runat="server" Text="Cantidad de llamadas al mismo tipo de equipo"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Label ID="samePhone" runat="server" Text=""></asp:Label>
                </div>
                <div class="col-xs-3"></div>
            </div>
                        <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Button ID="Button1" class="btn btn-info" runat="server" Text="Generar Informacion" OnClick="Button1_Click" />
                </div>
                <div class="col-xs-3">
                </div>
                <div class="col-xs-3"></div>
            </div>
        </div>
    </div>

    <div id="popup" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button4" class="btn btn-success" runat="server" Text="Button" />
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function ShowPopup(title, body) {
            $("#popup .modal-title").html(title);
            $("#popup .modal-body").html(body);
            $("#popup").modal("show");
        }
    </script>
</asp:Content>
