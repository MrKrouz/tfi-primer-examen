﻿<%@ Page Title="Gestión de cliente" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CallRegistry.aspx.cs" Inherits="WebUI.CallRegistry" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Gestion de Llamados</h2>
    <asp:HiddenField ID="HiddenField1" runat="server" />
    <div class="row">
        <div class="col-md-12">
            <p>
                <center>
                    En el siguiente formulario usted podra ingresar los registros de llamada manualmente.
                </center>
            </p>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="lblName" runat="server" Text="Origen"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList>
                </div>
                <div class="col-xs-3">
                </div>
                <div class="col-xs-3">
                </div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label1" runat="server" Text="Destino"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label2" runat="server" Text="Duracion (Minutos)"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-3">
                    <asp:Label ID="Label3" runat="server" Text="Fecha"></asp:Label>
                </div>
                <div class="col-xs-3">
                    <asp:Calendar ID="Calendar1" runat="server" EnableTheming="True" SelectedDate="09/22/2021 01:57:14">
                    </asp:Calendar>
                </div>
                <div class="col-xs-3"></div>
            </div>
            <div class="row" style="padding-top: 3em">
                <div class="col-xs-4">
                </div>
                <div class="col-xs-4">
                    <asp:Button ID="Button1" class="btn btn-primary" runat="server" Text="Generar nuevo registro" OnClick="Button1_Click" />
                </div>
                <div class="col-xs-4">
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-top: 3em">
        <div class="col-md-12">
            <div class="row" style="padding-top: 1em">
                <div class="col-xs-3"></div>
                <div class="col-xs-6">
                    <asp:GridView ID="GridView1" runat="server" class="table table-bordered table-condensed table-responsive table-hover" AutoGenerateColumns="False"  Width="100%">
                        <Columns>
                            <asp:BoundField HeaderText="ID" DataField="ID" ItemStyle-Width="2em" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center" Width="2em"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Usuario Origen" DataField="Origin.LastName" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Compañia Origen" DataField="Origin.Company.Name" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Usuario Destino" DataField="Destination.LastName" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Compañia Destino" DataField="Destination.Company.Name" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Fecha" DataField="Date" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                            <asp:BoundField HeaderText="Duracion (Minutos)" DataField="Duration" ItemStyle-HorizontalAlign="Center">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                            </asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
                <div class="col-xs-3">
                </div>
            </div>
        </div>
    </div>



    <div id="popup" class="modal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        &times;</button>
                    <h4 class="modal-title"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <asp:Button ID="Button4" class="btn btn-success" runat="server" Text="Button" />
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        function ShowPopup(title, body) {
            $("#popup .modal-title").html(title);
            $("#popup .modal-body").html(body);
            $("#popup").modal("show");
        }
    </script>
</asp:Content>
