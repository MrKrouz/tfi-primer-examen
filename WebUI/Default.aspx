﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebUI._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>Primer examen parcial TFI</h1>
        <p class="lead">En los botones de la navegacion ubicados en la parte superior tienen a mano cada uno de los ABMs asi como tambien las consultas requeridas.</p>
        <p>Tambien, en las secciones de abajo encontraran las referencias para ir a las secciones de informacion que ustedes requieran.</p>
    </div>

    <div class="row">
        <div class="col-md-6">
            <h2>Informacion relacionada a los clientes.</h2>
            <p>
                Aca encontraran toda la informacion relacionada a las llamadas realizadas por los clientes, asi como tambien sus tarifas.
            </p>
            <p>
                <a class="btn btn-default" href="~/#">Info Clientes &raquo;</a>
            </p>
        </div>
        <div class="col-md-6">
            <h2>Informacion relacionada a la compañia</h2>
            <p>
                Aca encontraran toda la informacion relacionada a la compañia.
            </p>
            <p>
                <a class="btn btn-default" href="~/#">Info Compañia &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
