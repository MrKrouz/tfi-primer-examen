﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebUI
{
    public partial class Contact : Page
    {
        BLL.Client_Logic client_Logic = new BLL.Client_Logic();
        string title = "Sistema";
        string body;

        protected void Page_Load(object sender, EventArgs e)
        {
            CargarCombos();
            CargarGrilla();
            CheckBox1.Enabled = false;
        }

        private void CargarGrilla()
        {

            GridView1.DataSource = client_Logic.GetAllClients();
            GridView1.DataBind();

        }


        private void CargarCombos()
        {
            var bll = new BLL.Company_Logic();


            DropDownList1.DataSource = bll.GetCompanies().Where(a => a.Active == true).ToList();
            DropDownList1.DataTextField = "Name";
            DropDownList1.DataValueField = "ID";
            DropDownList1.DataBind();

            DropDownList2.Items.Add(new ListItem { Value = 1.ToString(), Text = BE.ClientType.Celular.ToString() });
            DropDownList2.Items.Add(new ListItem { Value = 2.ToString(), Text = BE.ClientType.Linea.ToString() });
            DropDownList2.DataBind();


        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BE.Client client = new BE.Client();

            client.Name = TextBox1.Text;
            client.LastName = TextBox2.Text;
            client.Company = new BE.Company { ID = int.Parse(DropDownList1.SelectedValue) };
            client.ClientType = (BE.ClientType)Enum.Parse(typeof(BE.ClientType), DropDownList2.SelectedValue);
            if (client_Logic.Add(client) > 0)
            {
                body = "El cliente ha sido dado de alta exitosamente. Lo puede ver en la grilla de abajo.";
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
                CargarGrilla();
            }
            else
            {
                body = "El cliente no ha sido dado de alta. Por favor, rellene todos los cambios requeridos del formulario.";
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
            }
            ;
        }

        protected void ButtonModal_Click(object sender, EventArgs e)
        {

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = int.Parse(GridView1.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text);
            switch (e.CommandName)
            {
                case "Eliminar":
                    {
                        if (client_Logic.Delete(new BE.Client { ID = id }) > 0)
                        {
                            CargarGrilla();
                            body = "El cliente ha sido dado de baja exitosamente. El cambio se puede ver en la grilla de abajo.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);

                        }
                        else
                        {
                            body = "Error de sistema, el cliente no se ha dado de baja.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);

                        }
                        break;
                    }
                case "Seleccionar":
                    {
                        HiddenField1.Value = id.ToString();
                        BE.Client client = client_Logic.GetClientByID(id);
                        if (client != null)
                        {
                            TextBox1.Text = client.Name;
                            TextBox2.Text = client.LastName;
                            DropDownList1.SelectedValue = client.Company.ID.ToString();
                            DropDownList2.SelectedValue = ((int)client.ClientType).ToString();
                            CheckBox1.Checked = client.Active;
                        }
                        CheckBox1.Enabled = true;
                        break;
                    }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            BE.Client client = new BE.Client();
            client.ID = int.Parse(HiddenField1.Value);
            client.Name = TextBox1.Text;
            client.LastName = TextBox2.Text;
            client.Company = new BE.Company { ID = int.Parse(DropDownList1.SelectedValue) };
            client.ClientType = (BE.ClientType)Enum.Parse(typeof(BE.ClientType), DropDownList2.SelectedValue);
            client.Active = CheckBox1.Checked;

            if (client_Logic.Update(client) > 0)
            {
                CargarGrilla();
                body = "El cliente ha sido actualizado con exito. El cambio se vera en la grilla de abajo.";
            }
            else
            {
                body = "Error de sistema, el cliente no se ha modificado.";
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
        }
    }
}