﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebUI
{
    public partial class ClientInfo : System.Web.UI.Page
    {
        BLL.Registry_Logic registry_Logic = new BLL.Registry_Logic();
        BLL.Client_Logic client_Logic = new BLL.Client_Logic();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CargarCombo();
            }
        }

        private void CargarCombo()
        {
            DropDownList1.DataSource = client_Logic.GetActiveClients();
            DropDownList1.DataValueField = "ID";
            DropDownList1.DataTextField = "FullName";
            DropDownList1.DataBind();
        }

        private void ActualizarInfo()
        {


        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {


        }

        protected void DropDownList1_TextChanged(object sender, EventArgs e)
        {
            Dictionary<string, int> infoA = registry_Logic.GetAllCallsByClientID(int.Parse(DropDownList1.SelectedValue));
            double deuda = registry_Logic.GetMontlyDebtByClient(int.Parse(DropDownList1.SelectedValue));
            BE.Client client = client_Logic.GetClientByID(int.Parse(DropDownList1.SelectedValue));

            clientName.Text = client.Name;
            clientLastName.Text = client.LastName;
            clientMonthCharge.Text = deuda.ToString();
            international.Text = infoA["international"].ToString();
            sameCompany.Text = infoA["sameCompany"].ToString();
            otherType.Text = infoA["differentType"].ToString();
            samePhone.Text = infoA["differentCompany"].ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Dictionary<string, int> infoA = registry_Logic.GetAllCallsByClientID(int.Parse(DropDownList1.SelectedValue));
            double deuda = registry_Logic.GetMontlyDebtByClient(int.Parse(DropDownList1.SelectedValue));
            BE.Client client = client_Logic.GetClientByID(int.Parse(DropDownList1.SelectedValue));

            double porcentajeInternacional = 0;
            double porcentajeMismaCompañia = 0;
            double porcentajeDiferenteTipo = 0;
            double porcentajeCompañia = 0;
            int total = infoA["international"] + infoA["sameCompany"] + infoA["differentType"] + infoA["differentCompany"];
            if (total > 0)
            {
                porcentajeInternacional = infoA["international"] * 100 / total;
                porcentajeMismaCompañia = infoA["sameCompany"] * 100 / total;
                porcentajeDiferenteTipo = infoA["differentType"] * 100 / total;
                porcentajeCompañia = infoA["differentCompany"] * 100 / total;
            }


            clientName.Text = client.Name;
            clientLastName.Text = client.LastName;
            clientMonthCharge.Text = deuda.ToString();
            international.Text = porcentajeInternacional + "%";
            sameCompany.Text = porcentajeMismaCompañia + "%";
            otherType.Text = porcentajeDiferenteTipo + "%";
            samePhone.Text = porcentajeCompañia + "%";
        }
    }
}