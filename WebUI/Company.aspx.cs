﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebUI
{
    public partial class About : Page
    {
        BLL.Company_Logic company_Logic = new BLL.Company_Logic();
        string title = "Sistema";
        string body;

        protected void Page_Load(object sender, EventArgs e)
        {
            CargarGrilla();
            CheckBox1.Enabled = false;
        }

        private void CargarGrilla()
        {

            GridView1.DataSource = company_Logic.GetCompanies();
            GridView1.DataBind();

        }

        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int id = int.Parse(GridView1.Rows[int.Parse(e.CommandArgument.ToString())].Cells[0].Text);
            BLL.Client_Logic client_Logic = new BLL.Client_Logic();
            bool flag = false;


            switch (e.CommandName)
            {
                case "Eliminar":
                    {
                        foreach (var item in client_Logic.GetAllClients())
                        {
                            if (item.Company.ID == id)
                            {
                                body = "Error de sistema, todavia hay clientes relacionados con esta compañia. Primer contactese con el cliente y solicite su cambio de compañia.";
                                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
                                flag = true;
                                break;
                            }
                        }
                        if (flag == true)
                        {
                            break;
                        }

                        if (company_Logic.Delete(new BE.Company { ID = id }) > 0)
                        {
                            CargarGrilla();
                            body = "El cliente ha sido dado de baja exitosamente. El cambio se puede ver en la grilla de abajo.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);

                        }
                        else
                        {
                            body = "Error de sistema, el cliente no se ha dado de baja.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);

                        }
                        break;
                    }
                case "Seleccionar":
                    {
                        HiddenField1.Value = id.ToString();
                        BE.Company company = company_Logic.GetCompanyByID(id);
                        if (company != null)
                        {
                            TextBox1.Text = company.Name;
                            TextBox2.Text = company.Country;
                            CheckBox1.Checked = company.Active;
                        }
                        CheckBox1.Enabled = true;
                        break;
                    }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BE.Company company = new BE.Company();

            company.Name = TextBox1.Text;
            company.Country = TextBox2.Text;
            if (company_Logic.Add(company) > 0)
            {
                body = "La compañia ha sido dada de alta exitosamente. Lo puede ver en la grilla de abajo.";
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
                CargarGrilla();
            }
            else
            {
                body = "La compañia no ha sido dada de alta. Por favor, rellene todos los cambios requeridos del formulario.";
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            BE.Company company = new BE.Company();
            company.ID = int.Parse(HiddenField1.Value);
            company.Name = TextBox1.Text;
            company.Country = TextBox2.Text;
            company.Active = CheckBox1.Checked;

            if (company_Logic.Update(company) > 0)
            {
                CargarGrilla();
                body = "La compañia ha sido actualizada con exito. El cambio se vera en la grilla de abajo.";
            }
            else
            {
                body = "Error de sistema, la compañia no se ha modificado.";
            }

            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
        }
    }
}