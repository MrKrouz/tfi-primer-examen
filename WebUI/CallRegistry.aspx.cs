﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebUI
{
    public partial class CallRegistry : Page
    {
        string title = "Sistema";
        string body = "";
        BLL.Registry_Logic registry_Logic = new BLL.Registry_Logic();
        BLL.Client_Logic client_Logic = new BLL.Client_Logic();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CargarCombos();
                Calendar1.SelectedDate = System.DateTime.Now;
            }
            CargarGrilla();
        }

        private void CargarCombos()
        {
            
            DropDownList1.DataSource = client_Logic.GetActiveClients();
            DropDownList1.DataValueField = "ID";
            DropDownList1.DataTextField = "FullName";
            DropDownList1.DataBind();

            DropDownList2.DataSource = client_Logic.GetActiveClients();
            DropDownList2.DataValueField = "ID";
            DropDownList2.DataTextField = "FullName";
            DropDownList2.DataBind();
        }

        private void CargarGrilla()
        {
            GridView1.DataSource = registry_Logic.GetRegistries();
            GridView1.DataBind();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BE.CallRegistry callRegistry = new BE.CallRegistry();

            int duracion = 0;

            if(DropDownList1.SelectedValue != DropDownList2.SelectedValue)
            {
                if(int.TryParse(TextBox1.Text, out duracion))
                {
                    if (duracion > 0)
                    {
                        callRegistry.Origin = client_Logic.GetClientByID(int.Parse(DropDownList1.SelectedValue));
                        callRegistry.Destination = client_Logic.GetClientByID(int.Parse(DropDownList1.SelectedValue));
                        callRegistry.Duration = int.Parse(TextBox1.Text);
                        callRegistry.Date = Calendar1.SelectedDate;
                        if (registry_Logic.Add(callRegistry)>0)
                        {
                            body = "Se ha registrado la llamada. Puede ver los datos detallados abajo.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
                        }
                        else
                        {
                            body = "Error del sistema, el registro no se ha cargado.";
                            ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
                        }
                    }
                    else
                    {
                        body = "La duracion debe ser superior a 1 minuto para poder ser facturada.";
                        ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);

                    }
                }
                else
                {
                    body = "La duracion debe ser un valor numerico.";
                    ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
                }
            }
            else
            {
                body = "Error: El cliente no puede llamarse a si mismo.";
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('" + title + "','" + body + "');", true);
            }
        }
    }
}