﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebUI
{
    public partial class CompanyInfo : System.Web.UI.Page
    {
        BLL.Registry_Logic registry_Logic = new BLL.Registry_Logic();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                CargarCombo();
            }
        }

        private void CargarCombo()
        {
            DropDownList1.Items.Add(new ListItem { Value = "1", Text = "Enero" });
            DropDownList1.Items.Add(new ListItem { Value = "2", Text = "Febrero" });
            DropDownList1.Items.Add(new ListItem { Value = "3", Text = "Marzo" });
            DropDownList1.Items.Add(new ListItem { Value = "4", Text = "Abril" });
            DropDownList1.Items.Add(new ListItem { Value = "5", Text = "Mayo" });
            DropDownList1.Items.Add(new ListItem { Value = "6", Text = "Junio" });
            DropDownList1.Items.Add(new ListItem { Value = "7", Text = "Julio" });
            DropDownList1.Items.Add(new ListItem { Value = "8", Text = "Agosto" });
            DropDownList1.Items.Add(new ListItem { Value = "9", Text = "Septiembre" });
            DropDownList1.Items.Add(new ListItem { Value = "10", Text = "Octubre" });
            DropDownList1.Items.Add(new ListItem { Value = "11", Text = "Noviembre" });
            DropDownList1.Items.Add(new ListItem { Value = "12", Text = "Diciembre" });
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var datos = registry_Logic.GetTotalByMonth(int.Parse(DropDownList1.SelectedValue));
            var cantidades = registry_Logic.GetAllCallsByType();
            
            double porcentajeInternacional = 0;
            double porcentajeMismaCompañia = 0;
            double porcentajeDiferenteTipo = 0;
            double porcentajeCompañia = 0;

            int total = cantidades["international"] + cantidades["sameCompany"] + cantidades["differentType"] + cantidades["differentCompany"];
            
            if (total > 0)
            {
                porcentajeInternacional = cantidades["international"] * 100 / total;
                porcentajeMismaCompañia = cantidades["sameCompany"] * 100 / total;
                porcentajeDiferenteTipo = cantidades["differentType"] * 100 / total;
                porcentajeCompañia = cantidades["differentCompany"] * 100 / total;
            }

            sameCompanyPercentage.Text = porcentajeMismaCompañia.ToString() + " " + "%";
            otherPhonePercentage.Text = porcentajeDiferenteTipo.ToString() + " " + "%";
            samePhonePercentage.Text = porcentajeCompañia.ToString() + " " + "%";
            internationalPercentage.Text = porcentajeInternacional.ToString() + " " + "%";

            sameCompany.Text = datos["sameCompany"].ToString();
            otherPhone.Text = datos["differentType"].ToString();
            samePhone.Text = datos["differentCompany"].ToString();
            international.Text = datos["international"].ToString();

            totalProfit.Text = datos["total"].ToString();

        }
    }
}