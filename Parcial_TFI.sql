USE [Master]
create database PrimerParcialTFI
USE [PrimerParcialTFI]
GO
/****** Object:  Table [dbo].[CallRegistry]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CallRegistry](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[OriginID] [int] NULL,
	[DestinationID] [int] NULL,
	[Date] [datetime] NULL,
	[Duration] [int] NULL,
 CONSTRAINT [PK_CallRegistry] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Client]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[LastName] [nvarchar](max) NULL,
	[CompanyID] [int] NULL,
	[Active] [bit] NULL CONSTRAINT [DF_Client_Active]  DEFAULT ((1)),
	[ClientTypeID] [int] NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ClientType]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClientType](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](max) NULL,
 CONSTRAINT [PK_ClientType] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Company]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Country] [nvarchar](max) NULL,
	[Active] [bit] NULL CONSTRAINT [DF_Company_Active]  DEFAULT ((1)),
 CONSTRAINT [PK_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[CallRegistry] ON 

INSERT [dbo].[CallRegistry] ([ID], [OriginID], [DestinationID], [Date], [Duration]) VALUES (1, 7, 6, CAST(N'2021-09-21 00:21:08.617' AS DateTime), 3)
INSERT [dbo].[CallRegistry] ([ID], [OriginID], [DestinationID], [Date], [Duration]) VALUES (2, 8, 6, CAST(N'2021-09-21 00:21:42.120' AS DateTime), 5)
INSERT [dbo].[CallRegistry] ([ID], [OriginID], [DestinationID], [Date], [Duration]) VALUES (3, 9, 6, CAST(N'2021-09-21 00:21:44.083' AS DateTime), 7)
INSERT [dbo].[CallRegistry] ([ID], [OriginID], [DestinationID], [Date], [Duration]) VALUES (4, 7, 6, CAST(N'2021-09-21 00:21:45.317' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[CallRegistry] OFF
SET IDENTITY_INSERT [dbo].[Client] ON 

INSERT [dbo].[Client] ([ID], [Name], [LastName], [CompanyID], [Active], [ClientTypeID]) VALUES (6, N'Pepe', N'Gomez', 2, 1, 1)
INSERT [dbo].[Client] ([ID], [Name], [LastName], [CompanyID], [Active], [ClientTypeID]) VALUES (7, N'Roberto', N'Gomez', 2, 1, 2)
INSERT [dbo].[Client] ([ID], [Name], [LastName], [CompanyID], [Active], [ClientTypeID]) VALUES (8, N'Joan', N'Lopez', 4, 1, 1)
INSERT [dbo].[Client] ([ID], [Name], [LastName], [CompanyID], [Active], [ClientTypeID]) VALUES (9, N'Matias', N'Sanchez', 2, 1, 1)
SET IDENTITY_INSERT [dbo].[Client] OFF
SET IDENTITY_INSERT [dbo].[ClientType] ON 

INSERT [dbo].[ClientType] ([ID], [Type]) VALUES (1, N'Cellphone')
INSERT [dbo].[ClientType] ([ID], [Type]) VALUES (2, N'Phone')
SET IDENTITY_INSERT [dbo].[ClientType] OFF
SET IDENTITY_INSERT [dbo].[Company] ON 

INSERT [dbo].[Company] ([ID], [Name], [Country], [Active]) VALUES (2, N'Movistar', N'Argentina', 1)
INSERT [dbo].[Company] ([ID], [Name], [Country], [Active]) VALUES (3, N'Personal', N'Argentina', 1)
INSERT [dbo].[Company] ([ID], [Name], [Country], [Active]) VALUES (4, N'Claro', N'España', 1)
SET IDENTITY_INSERT [dbo].[Company] OFF
ALTER TABLE [dbo].[CallRegistry]  WITH CHECK ADD  CONSTRAINT [FK_CallRegistry_Client] FOREIGN KEY([OriginID])
REFERENCES [dbo].[Client] ([ID])
GO
ALTER TABLE [dbo].[CallRegistry] CHECK CONSTRAINT [FK_CallRegistry_Client]
GO
ALTER TABLE [dbo].[CallRegistry]  WITH CHECK ADD  CONSTRAINT [FK_CallRegistry_Client1] FOREIGN KEY([DestinationID])
REFERENCES [dbo].[Client] ([ID])
GO
ALTER TABLE [dbo].[CallRegistry] CHECK CONSTRAINT [FK_CallRegistry_Client1]
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_ClientType] FOREIGN KEY([ClientTypeID])
REFERENCES [dbo].[ClientType] ([ID])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_ClientType]
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_Company] FOREIGN KEY([CompanyID])
REFERENCES [dbo].[Company] ([ID])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_Company]
GO
/****** Object:  StoredProcedure [dbo].[createClient]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[createClient](
@name nvarchar(max),
@lastName nvarchar(max),
@companyID int,
@clientType int
)
as
begin
insert into Client (Name, LastName, CompanyID, ClientTypeID)
values (@name, @lastName, @companyID, @clientType)
end

GO
/****** Object:  StoredProcedure [dbo].[createCompany]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[createCompany]
(
@name nvarchar(max),
@country nvarchar(max)
)
as
begin
insert into Company (Name, Country)
values (@name, @country)
end

GO
/****** Object:  StoredProcedure [dbo].[createRegistry]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[createRegistry](
@originID int,
@destinationID int,
@duration int,
@date datetime
)
as
begin
insert into CallRegistry (OriginID, DestinationID, Duration, Date)
values (@originID, @destinationID, @duration, @date)
end

GO
/****** Object:  StoredProcedure [dbo].[deleteClient]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[deleteClient](
@ID int
)
as
begin
update Client set Active = 0
where ID = @ID
end

GO
/****** Object:  StoredProcedure [dbo].[deleteCompany]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[deleteCompany]
(
@ID int
)
as
begin
update Company set Active = 0
where ID = @ID
end

GO
/****** Object:  StoredProcedure [dbo].[getClients]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getClients]
as
begin
select ID, Name, LastName, CompanyID, Active , ClientTypeID
from Client
end

GO
/****** Object:  StoredProcedure [dbo].[getCompanies]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getCompanies]
as
begin
select ID, Name, Country, Active from Company
end

GO
/****** Object:  StoredProcedure [dbo].[getRegistries]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[getRegistries]
as
begin
select ID, Date, Duration, OriginID, DestinationID
from CallRegistry
end

GO
/****** Object:  StoredProcedure [dbo].[updateClient]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[updateClient](
@ID int,
@name nvarchar(max),
@lastName nvarchar(max),
@companyID int,
@active bit,
@clientType int
)
as
begin
update Client set Name = @name, 
LastName = @lastName, 
CompanyID = @companyID,
ClientTypeID = @clientType,
Active = @active
where ID = @ID
end

GO
/****** Object:  StoredProcedure [dbo].[updateCompany]    Script Date: 22/9/2021 16:50:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[updateCompany]
(
@ID int,
@name nvarchar(max),
@country nvarchar(max),
@active bit
)
as
begin
update Company set Name = @name, Country = @country, Active = @active
where ID = @ID
end

GO
