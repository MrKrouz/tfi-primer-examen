﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class Client
    {
        public int ID {get;set;}
        public string Name { get; set; }
        public string LastName { get; set; }
        public Company Company { get; set; } = new Company();
        public bool Active { get; set; }
        public ClientType ClientType { get; set; }

        public string FullName 
        { get
            {
                return Name + " " + LastName;
            }
        }

    }

}
