﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class CallRegistry
    {
        public int ID { get; set; }
        public Client Origin { get; set; } = new Client();
        public Client Destination { get; set; } = new Client();
        public DateTime Date { get; set; }
        public int Duration { get; set; }
    }
}
