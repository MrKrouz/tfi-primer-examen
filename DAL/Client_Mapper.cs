﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class Client_Mapper
    {
        private Access access = new Access();

        public int Create(BE.Client client)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@name", client.Name));
            sqlParameters.Add(access.createParameter("@lastName", client.LastName));
            sqlParameters.Add(access.createParameter("@companyID", client.Company.ID));
            sqlParameters.Add(access.createParameter("@clientType", (int)client.ClientType));
            return access.Write("createClient", sqlParameters);
        }

        public List<BE.Client> Read()
        {
            DataTable dataTable = new DataTable();
            List<BE.Client> clients = new List<BE.Client>();

            dataTable = access.Read("getClients");

            foreach(DataRow dataRow in dataTable.Rows)
            {
                BE.Client client = new BE.Client();
                client.ID = int.Parse(dataRow[0].ToString());
                client.Name = dataRow[1].ToString();
                client.LastName = dataRow[2].ToString();
                client.Company.ID = int.Parse(dataRow[3].ToString());
                client.Active = Convert.ToBoolean(dataRow[4]);
                client.ClientType = (BE.ClientType)Enum.Parse(typeof(BE.ClientType), dataRow[5].ToString());
                clients.Add(client);
            }

            return clients;
        }

        public int Update(BE.Client client)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@ID", client.ID));
            sqlParameters.Add(access.createParameter("@name", client.Name));
            sqlParameters.Add(access.createParameter("@lastName", client.LastName));
            sqlParameters.Add(access.createParameter("@companyID", client.Company.ID));
            sqlParameters.Add(access.createParameter("@clientType", (int)client.ClientType));
            sqlParameters.Add(access.createParameter("@active", client.Active));
            return access.Write("updateClient", sqlParameters);
        }

        public int Delete(BE.Client client)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@ID", client.ID));
            return access.Write("deleteClient", sqlParameters);
        }
    }
}
