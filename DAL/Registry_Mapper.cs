﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Registry_Mapper
    {

        private Access access = new Access();

        public int Create(BE.CallRegistry callRegistry)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@originID", callRegistry.Origin.ID));
            sqlParameters.Add(access.createParameter("@destinationID", callRegistry.Destination.ID));
            sqlParameters.Add(access.createParameter("@duration", callRegistry.Duration));
            sqlParameters.Add(access.createParameter("@date", callRegistry.Date));
            return access.Write("createRegistry", sqlParameters);
        }

        public List<BE.CallRegistry> Read()
        {
            DataTable dataTable = new DataTable();
            List<BE.CallRegistry> registries = new List<BE.CallRegistry>();

            dataTable = access.Read("getRegistries");

            foreach (DataRow dataRow in dataTable.Rows)
            {
                BE.CallRegistry callRegistry = new BE.CallRegistry();
                callRegistry.ID = int.Parse(dataRow[0].ToString());
                callRegistry.Date = DateTime.Parse(dataRow[1].ToString());
                callRegistry.Duration = int.Parse(dataRow[2].ToString());
                callRegistry.Origin.ID = int.Parse(dataRow[3].ToString());
                callRegistry.Destination.ID = int.Parse(dataRow[4].ToString());
                registries.Add(callRegistry);
            }

            return registries;
        }
    }
}
