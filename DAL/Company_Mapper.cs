﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Company_Mapper
    {
        private Access access = new Access();

        public int Create(BE.Company company)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@name", company.Name));
            sqlParameters.Add(access.createParameter("@country", company.Country));
            return access.Write("createCompany", sqlParameters);
        }

        public List<BE.Company> Read()
        {
            DataTable dataTable = new DataTable();
            List<BE.Company> companies = new List<BE.Company>();

            dataTable = access.Read("getCompanies");

            foreach (DataRow dataRow in dataTable.Rows)
            {
                BE.Company company = new BE.Company();
                company.ID = int.Parse(dataRow[0].ToString());
                company.Name = dataRow[1].ToString();
                company.Country = dataRow[2].ToString();
                company.Active = bool.Parse(dataRow[3].ToString());
                companies.Add(company);
            }

            return companies;
        }

        public int Update(BE.Company company)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@ID", company.ID));
            sqlParameters.Add(access.createParameter("@name", company.Name));
            sqlParameters.Add(access.createParameter("@country", company.Country));
            sqlParameters.Add(access.createParameter("@active", company.Active));
            return access.Write("updateCompany", sqlParameters);
        }

        public int Delete(BE.Company company)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(access.createParameter("@ID", company.ID));
            return access.Write("deleteCompany", sqlParameters);
        }

    }
}
