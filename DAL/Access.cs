﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;


namespace DAL
{
    internal class Access
    {
        private SqlConnection sqlConnection = new SqlConnection();

        private void openConnection()
        {
            sqlConnection.ConnectionString = "Data Source=localhost;Initial Catalog=PrimerParcialTFI;Integrated Security=True";
            sqlConnection.Open();
        }

        private void closeConnection()
        {
            sqlConnection.Close();
        }

        private SqlCommand GetSqlCommand(string name, List<SqlParameter> sqlParameters = null)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = sqlConnection;
            sqlCommand.CommandText = name;
            sqlCommand.CommandType = CommandType.StoredProcedure;

            if (sqlParameters != null)
            {
                if (sqlParameters.Count > 0)
                {
                    sqlCommand.Parameters.AddRange(sqlParameters.ToArray());
                }
            }

            return sqlCommand;

        }

        public int Write(string name, List<SqlParameter> sqlParameters)
        {
            int affectedRows = 0;

            SqlCommand sqlCommand = GetSqlCommand(name, sqlParameters);
            try
            {
                openConnection();
                affectedRows = sqlCommand.ExecuteNonQuery();
            }
            catch
            {
                affectedRows = -1;
            }

            closeConnection();
            return affectedRows;
        }

        public DataTable Read(string name)
        {
            DataTable dataTable = new DataTable();
            SqlDataAdapter sqlDataAdapter = new SqlDataAdapter();
            try
            {
                openConnection();
                sqlDataAdapter.SelectCommand = GetSqlCommand(name);
                sqlDataAdapter.Fill(dataTable);
                sqlDataAdapter = null;
                closeConnection();
                return dataTable;
            }
            catch
            {
                closeConnection();
                return dataTable;
            }
        }

        #region Parameter Creation
        public SqlParameter createParameter(string name, string value)
        {
            SqlParameter sqlParameter = new SqlParameter();
            sqlParameter.ParameterName = name;
            sqlParameter.Value = value;
            sqlParameter.SqlDbType = SqlDbType.NVarChar;
            return sqlParameter;
        }

        public SqlParameter createParameter(string name, bool value)
        {
            SqlParameter sqlParameter = new SqlParameter();
            sqlParameter.ParameterName = name;
            sqlParameter.Value = value;
            sqlParameter.SqlDbType = SqlDbType.Bit;
            return sqlParameter;
        }

        public SqlParameter createParameter(string name, DateTime value)
        {
            SqlParameter sqlParameter = new SqlParameter();
            sqlParameter.ParameterName = name;
            sqlParameter.Value = value;
            sqlParameter.SqlDbType = SqlDbType.DateTime;
            return sqlParameter;
        }

        public SqlParameter createParameter(string name, int value)
        {
            SqlParameter sqlParameter = new SqlParameter();
            sqlParameter.ParameterName = name;
            sqlParameter.Value = value;
            sqlParameter.SqlDbType = SqlDbType.Int;
            return sqlParameter;
        }
        #endregion


    }
}
